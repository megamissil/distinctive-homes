<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

			</section>
		</div>
		<div id="footer-container">
			<footer id="footer">
				<div class="row">
					<div class="ft-info">
						<?php dynamic_sidebar( 'footer-widgets' ); ?>
						<p>Copyright &copy; <?=date('Y'); ?> Distinctive Homes of Alabama. All Rights Reserved.</p>
					</div>
					<div class="ft-moxy">
						<a href="www.digmoxy.com" target="_blank">Moxy</a>
					</div>
				</div>
				
			</footer>
		</div>

		<?php do_action( 'foundationpress_layout_end' ); ?>

<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) == 'offcanvas' ) : ?>
		</div><!-- Close off-canvas wrapper inner -->
	</div><!-- Close off-canvas wrapper -->
</div><!-- Close off-canvas content wrapper -->
<?php endif; ?>

<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/components/slick/slick.min.js"></script>
<?php wp_footer(); ?>
<?php do_action( 'foundationpress_before_closing_body' ); ?>
</body>
</html>
