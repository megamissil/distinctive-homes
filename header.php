<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<!-- Favicon -->
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/favicon-16x16.png">
		<link rel="manifest" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">

		<link href="https://fonts.googleapis.com/css?family=Copse|Work+Sans:400,500,600,700" rel="stylesheet">

		<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/components/slick/slick.css">
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
	<?php do_action( 'foundationpress_after_body' ); ?>

	<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) == 'offcanvas' ) : ?>
	<div class="off-canvas-wrapper">
		<div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
		<?php get_template_part( 'template-parts/mobile-off-canvas' ); ?>
	<?php endif; ?>

	<?php do_action( 'foundationpress_layout_start' ); ?>

	<header id="masthead" class="site-header" role="banner">
		<div class="title-bar" data-responsive-toggle="site-navigation">
			<div class="mobile-nav-icon">
				<a href="javascript:;"><i class="fa fa-bars"></i></a>
			</div>
			<div class="mobile-nav">
				<i class="fa fa-times close"></i>
				<div class="mobile-nav-links">
					<?php wp_nav_menu( array('menu' => 'Mobile' )); ?>
				</div>
			</div>
			<div class="title-bar-title">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/dha-logo-new-web.png" alt="logo"></a>
			</div>
		</div>


		<div class="top-bar">
			<div class="top-bar-left">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/dha-logo-new-web.png" alt="logo">
				</a>
			</div>

			<!-- Small Right Top Bar -->
			<div class="top-bar-right hide-for-large">
				<ul>
					<li><a href="tel:13346579253 ">334.657.9253</a></li>
				</ul>
				<ul>
					<li><a href="http://www.houzz.com/pro/gary8461/distinctive-homes-of-alabama" target="_blank"><i class="fa fa-houzz" aria-hidden="true"></i></a></li>
					<li><a href="https://www.instagram.com/distinctivehomesofal/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
					<li><a href="https://www.pinterest.com/distinctivehom/" target="_blank"><i class="fa fa-pinterest-square" aria-hidden="true"></i></a></li>
					<li><a href="https://www.facebook.com/distinctivehomesofalabama/" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
					<li><a href="mailto:gary8461@gmail.com" target="_blank"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
				</ul>
			</div>

			<!-- Large Right Top Bar -->
			<div class="top-bar-right show-for-large">
				<ul>
					<li><a href="tel:13346579253 ">334.657.9253</a></li>
					<li><a href="http://www.houzz.com/pro/gary8461/distinctive-homes-of-alabama" target="_blank"><i class="fa fa-houzz" aria-hidden="true"></i></a></li>
					<li><a href="https://www.instagram.com/distinctivehomesofal/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
					<li><a href="https://www.pinterest.com/distinctivehom/" target="_blank"><i class="fa fa-pinterest-square" aria-hidden="true"></i></a></li>
					<li><a href="https://www.facebook.com/distinctivehomesofalabama/" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
					<li><a href="mailto:gary8461@gmail.com" target="_blank"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
				</ul>
			</div>
		</div>
	</header>
	
	<div class="row">
		<section class="container">
			<nav id="site-navigation" class="main-navigation" role="navigation">
				<div class="main-nav">
					<?php foundationpress_main_nav(); ?>
				</div>
			</nav>
			<?php do_action( 'foundationpress_after_header' );