
$(document).ready(function(){
  $('.home-slider').slick({
	autoplay: true,
	fade: true,
	arrows: false
  });
});

$(document).ready(function(){
  $('.floorplan-slider').slick({
	autoplay: true,
	fade: true,
	infinite: true,
  	slidesToShow: 4,
  	slidesToScroll: 1,
  	prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-chevron-left"></i></button>',
  	nextArrow: '<button type="button" class="slick-next"><i class="fa fa-chevron-right"></i></button>'
  });
});

$(document).ready(function(){
  $('.testimonials-slider').slick({
	autoplay: true,
	fade: true,
	infinite: true,
  	slidesToShow: 1,
  	slidesToScroll: 1,
  	prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-chevron-left"></i></button>',
  	nextArrow: '<button type="button" class="slick-next"><i class="fa fa-chevron-right"></i></button>'
  });
});