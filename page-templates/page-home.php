<?php
/*
Template Name: Home
*/
get_header(); ?>


<div id="page-full-width" role="main">
  	<article id="post-<?php the_ID(); ?>">
      	<div class="homepage">
			<div class="home-slider-container">
	        	<div class="home-slider">
			     	<?php $slider = new WP_Query( array( 'post_type' => 'home-slider', 'posts_per_page' => -1 ) );
			     	while( $slider->have_posts() ) : $slider->the_post(); ?>
			       		<div id="post-<?php the_ID(); ?>">
			          		<?php echo types_render_field( "home-slider-image", array( "alt" => "home-slider-image" ) ) ?>
			       		</div>
			     	<?php endwhile; wp_reset_query(); ?>
			  	</div>
			</div>
			<div class="home-content">
				<?php the_content(); ?>
			</div>
      	</div>
  	</article>
</div>

<?php get_footer();