<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

  get_header(); ?>

  

  <div id="page" role="main">
    <header>
      <!-- Contact Page Header -->
      <?php if ( is_page('contact-us') ) { ?>
        <div class="row">
          <div class="medium-6 columns">
            <?php dynamic_sidebar( 'contact-widget' ); ?>
          </div>
          <div class="medium-6 columns">
            <h2 class="entry-title"><?php the_title(); ?></h2>
          </div>
        </div>
      <?php } else { ?>
        <h2 class="entry-title"><?php the_title(); ?></h2>
      <?php } ?>
    </header>

    <?php while ( have_posts() ) : the_post(); ?>
      <article id="post-<?php the_ID(); ?>">

        <!-- Photo Gallery -->
        <?php if ( is_page('photo-gallery') ) { ?>
          <div class="entry-content">
            <?php the_content(); ?>
          </div>

        <!-- Floorplans -->
        <?php } elseif ( is_page('homes-floorplans') ) { ?>
          <div class="row">
            <?php
              $fpslider = new WP_Query( array( 'post_type' => 'floorplan', 'posts_per_page' => -1 ) );
              $i = 0;
              while( $fpslider->have_posts() ) : $fpslider->the_post(); 
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
                $image = $image[0]; ?>  
                <div class="floorplan-col" id="<?=$i; ?>" style="background-image: url('<?php echo $image ?>')">
                  <div id="post-<?php the_ID(); ?>" class="floorplan-bg">
                    <div class="floorplan-content">
                      <h2><?php the_title(); ?></h2>
                      <h5><?php echo types_render_field( "square-ft" ) ?></h5>
                      <a href="<?php echo types_render_field( "information-sheet", array( ) ) ?>" class="button" target="_blank">Information Sheet
                     </a>
                    </div>
                  </div>
                </div>
              <?php $i++; ?>
            <?php endwhile; wp_reset_query(); ?>
          </div>

        <!-- Testimonials --> 
        <?php } elseif ( is_page('what-people-are-saying') ) { ?>
          <div class="row">
            <div class="testimonials-container">
              <div class="medium-8 medium-centered columns">
                  <div class="testimonials-slider">
                    <?php $tslider = new WP_Query( array( 'post_type' => 'testimonial', 'posts_per_page' => -1 ) );
                    while( $tslider->have_posts() ) : $tslider->the_post(); ?>
                        <div id="post-<?php the_ID(); ?>">
                          <h3><?php the_title(); ?></h3>
                          <?php if (types_render_field('designer', array('output'=>'true'))) { ?>
                            <h5><span>Designer</span>: <?php echo types_render_field( "designer" ) ?></h5>
                          <?php } ?>
                          <hr>
                          <p><?php the_content(); ?></p>
                          
                        </div>
                    <?php endwhile; wp_reset_query(); ?>
                  </div>
                </div>
            </div>
          </div>

          <!-- Contact Us -->
          <?php } elseif ( is_page('contact-us') ) { ?>
          <div class="row contact-content">
              <div class="small-10 medium-8 large-6 small-centered columns">
                <?php the_content(); ?>
              </div>
          </div>


        <?php } else { ?>
          <div class="page-content" data-equalizer data-equalize-on="medium">
          <?php
          // // If a feature image is set, get the id, so it can be injected as a css background property
          if ( has_post_thumbnail( $post->ID ) ) :
            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
            $image = $image[0];
            ?>  
            <div class="page-featured" id="featured-hero" style="background-image: url('<?php echo $image ?>')" data-equalizer-watch>
          <?php endif; ?>

            </div>
            <div class="entry-content" data-equalizer-watch>
              <?php the_content(); ?>
            </div>
          </div>
        <?php } ?>
      </article>
    <?php endwhile;?>
  </div>

 <?php get_footer();
